import { Component } from "react";

class BodyInput extends Component {
  onInputChangeHandler(event) {
    console.log("Input được nhập");
    console.log(event.target.value);
  }

  onButtonGuiHandler() {
    console.log("Nút Gửi được bấm");
  }

  render() {
    return (
      <>
        <div className="row mt-4">
          <label className="form-label">Message cho bạn 12 tháng tới:</label>
          <input
            onChange={this.onInputChangeHandler}
            placeholder="Nhập message của bạn vào đây"
            className="form-control"
          />
        </div>
        <div className="row mt-3">
          <button className="btn btn-success m-auto" style={{ width: "150px" }}
            onClick={this.onButtonGuiHandler}
          >
            Gửi thông điệp
          </button>
        </div>
      </>
    );
  }
}

export default BodyInput;
