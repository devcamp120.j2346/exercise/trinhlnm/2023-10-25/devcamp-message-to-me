import { Component } from "react";

class BodyOutput extends Component {
  render() {
    return (
      <>
        <div className="row mt-3 text-primary">
          <p>Thông điệp của bạn ở đây</p>
        </div>
        <div className="row mt-3 mb-5">
          <img
            src={require("../../../assets/images/green-thumbs-up-png.png")}
            style={{ width: "100px", margin: "auto" }}
          />
        </div>
      </>
    );
  }
}

export default BodyOutput;
