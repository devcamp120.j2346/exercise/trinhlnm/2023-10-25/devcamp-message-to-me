import { Component } from "react";
import BodyInput from "./body-input/bodyInput";
import BodyOutput from "./body-output/bodyOutput";

class Body extends Component {
  render() {
    return (
        <>
            <BodyInput/>
            <BodyOutput/>
        </>
    );
  }
}

export default Body;