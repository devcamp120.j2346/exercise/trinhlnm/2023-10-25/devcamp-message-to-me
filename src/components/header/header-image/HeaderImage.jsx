import { Component } from "react";

class HeaderImage extends Component {
  render() {
    return (
      <div className="row mt-3">
        <img src={require("../../../assets/images/bg.jpg")} width="800px"/>
      </div>
    );
  }
}

export default HeaderImage;
